package lesson4.tests;

import lesson4.models.CartData;
import lesson4.models.OrderData;
import lesson4.models.ProductData;
import org.junit.Assert;
import org.junit.Test;

public class CartTests extends TestBase {
    @Test
    public void addProductToCartTest() {
        // given
        ProductData givenProductData = new ProductData("Faded Short Sleeve T-shirts");
        OrderData givenOrderData = new OrderData(10, OrderData.Size.M);
        loginIfNeeded();
        app.navigation.openHomePage();
        app.navigation.openProductPage(givenProductData);

        // when
        app.orders.selectProductParameters(givenOrderData);
        app.orders.clickAddToCart();

        // then
        app.navigation.openCartPage();
        CartData cartData = app.cart.getCartData();
        Assert.assertEquals(cartData.productData.name, givenProductData.name);
        Assert.assertEquals(cartData.orderData.quantity, givenOrderData.quantity);
        Assert.assertEquals(cartData.orderData.size, givenOrderData.size);
    }

    @Test
    public void deleteProductFromCartTest() {
        // given
        ProductData productData = new ProductData("Faded Short Sleeve T-shirts");
        loginIfNeeded();
        app.navigation.openCartPage();

        // when
        boolean success = app.cart.deleteProductFromCart(productData);

        // then
        if (!success) {
            Assert.fail(String.format("Товар \"%s\" не найден в корзине", productData.name));
        }
        Assert.assertFalse(app.cart.isProductInCart(productData));
    }
}
