package lesson6.tests;

import org.junit.jupiter.api.BeforeAll;

public class AuthBase extends TestBase {
    @Override
    @BeforeAll
    protected void setUp() {
        super.setUp();
        loginIfNeeded();
    }
}
