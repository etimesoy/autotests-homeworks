package lesson6;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import lesson6.models.AccountData;
import lesson6.models.SettingsData;

import java.io.File;
import java.io.IOException;

public class Settings {
    public static String file = "settings.json";
    private static final ObjectMapper objectMapper;
    private static final SettingsData settingsData;

    static {
        File file = new File(Settings.file);
        if (!file.exists()) {
            String description = String.format("Не найден файл с тестовыми данными по пути %s", Settings.file);
            throw new RuntimeException(description);
        }
        objectMapper = new ObjectMapper();
        try {
            settingsData = objectMapper.readValue(file, SettingsData.class);
        } catch (IOException e) {
            String description = String.format("Не удалось распарсить файл %s в экземпляр класса SettingsData", Settings.file);
            throw new RuntimeJsonMappingException(description);
        }
    }

    public static String getBaseUrl() {
        return settingsData.baseUrl;
    }

    public static AccountData getAccountData() {
        return settingsData.accountData;
    }
}
