package lesson4.helpers;

import lesson4.ApplicationManager;
import lesson4.models.AccountData;
import org.openqa.selenium.By;

public class LoginHelper extends HelperBase {
    public LoginHelper(ApplicationManager manager) {
        super(manager);
    }

    public void login(AccountData user) {
        findElement(By.id("email")).sendKeys(user.email);
        findElement(By.id("passwd")).sendKeys(user.password);
        clickElement(By.cssSelector("#SubmitLogin > span"));
    }

    public boolean isLogged() {
        return isElementPresent(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a/span"));
    }

    public String getCurrentAccountEmail() {
        return getInputElementText(By.id("email"));
    }
}
