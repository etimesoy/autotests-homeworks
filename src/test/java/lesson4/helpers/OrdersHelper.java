package lesson4.helpers;

import lesson4.ApplicationManager;
import lesson4.models.OrderData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class OrdersHelper extends HelperBase {
    public OrdersHelper(ApplicationManager manager) {
        super(manager);
    }

    public void selectProductParameters(OrderData orderData) {
        WebElement quantityInputField = findElement(By.id("quantity_wanted"));
        quantityInputField.clear();
        quantityInputField.sendKeys(orderData.quantity.toString());

        WebElement sizeSelectField = findElement(By.id("group_1"));
        sizeSelectField.click();
        new Select(sizeSelectField).selectByVisibleText(orderData.size.name());

        clickElement(By.id("color_14"));
    }

    public void clickAddToCart() {
        clickElement(By.xpath("//p[@id='add_to_cart']/button/span"));
        waitForElementToBeClickable(By.xpath("//*[@id='layer_cart']/div[1]/div[2]/div[4]/a"));
    }
}
