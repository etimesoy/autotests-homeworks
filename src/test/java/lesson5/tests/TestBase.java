package lesson5.tests;

import lesson5.ApplicationManager;
import lesson5.models.AccountData;
import org.junit.jupiter.api.BeforeEach;

public class TestBase {
    protected ApplicationManager app;
    protected AccountData defaultUser;

    @BeforeEach
    public void setUp() {
        app = ApplicationManager.getInstance();
        defaultUser = new AccountData("ruslangazizov21@mail.ru", "etimesoy", "123456");
    }

    protected void loginIfNeeded() {
        loginIfNeeded(defaultUser);
    }

    protected void loginIfNeeded(AccountData user) {
        if (!app.auth.isLogged()) {
            app.navigation.openLoginPageIfNeeded();
            app.auth.login(user);
        }
    }
}
