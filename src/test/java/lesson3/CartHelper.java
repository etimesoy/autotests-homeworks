package lesson3;

import org.openqa.selenium.By;

public class CartHelper extends HelperBase {
    CartHelper(ApplicationManager manager) {
        super(manager);
    }

    public void addCurrentProductToCart() {
        clickElement(By.cssSelector(".exclusive > span"));
    }
}
