package lesson3;

import org.openqa.selenium.By;

public class LoginHelper extends HelperBase {
    LoginHelper(ApplicationManager manager) {
        super(manager);
    }

    public void login(AccountData user) {
        findElement(By.id("email")).sendKeys(user.username);
        findElement(By.id("passwd")).sendKeys(user.password);
        clickElement(By.cssSelector("#SubmitLogin > span"));
    }
}
