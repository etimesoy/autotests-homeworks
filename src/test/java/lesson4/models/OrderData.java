package lesson4.models;

public class OrderData {
    public Integer quantity;
    public Size size;
    public enum Size {
        S, M, L
    }

    public OrderData(Integer quantity, Size size) {
        this.quantity = quantity;
        this.size = size;
    }
}
