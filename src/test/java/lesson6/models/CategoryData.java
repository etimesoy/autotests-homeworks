package lesson6.models;

import java.util.Objects;

public class CategoryData {
    public String name;

    public CategoryData(String name) {
        this.name = name;
    }

    public CategoryData() {
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (getClass() != obj.getClass()) return false;
        CategoryData other = (CategoryData) obj;
        return Objects.equals(name, other.name);
    }
}
