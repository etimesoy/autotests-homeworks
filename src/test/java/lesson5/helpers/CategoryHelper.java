package lesson5.helpers;

import lesson5.ApplicationManager;
import lesson5.models.CategoryData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Optional;

public class CategoryHelper extends HelperBase {
    public CategoryHelper(ApplicationManager manager) {
        super(manager);
    }

    public void addCategory(CategoryData categoryData) {
        findElement(By.id("property_1")).sendKeys(categoryData.name);
        clickElement(By.id("submit"));
    }

    public ArrayList<CategoryData> getCategories() {
        WebElement tbody = findElement(By.id("limits-tbody"));
        ArrayList<CategoryData> categories = new ArrayList<>();
        for (WebElement tr : tbody.findElements(By.xpath("./tr"))) {
            String name = tr.findElement(By.xpath("./td[2]")).getText();
            categories.add(new CategoryData(name));
        }
        return categories;
    }

    public boolean deleteCategory(CategoryData categoryData) {
        Optional<WebElement> tr = getCategoryWebElement(categoryData);
        tr.ifPresent(element -> element.findElement(By.xpath("./td[5]/a")).click());
        return tr.isPresent();
    }

    public boolean isCategoryPresent(CategoryData categoryData) {
        driver.navigate().refresh();
        return getCategoryWebElement(categoryData).isPresent();
    }

    // Private

    private Optional<WebElement> getCategoryWebElement(CategoryData categoryData) {
        WebElement tbody = findElement(By.id("limits-tbody"));
        for (WebElement tr : tbody.findElements(By.xpath("./tr"))) {
            String name = tr.findElement(By.xpath("./td[2]")).getText();
            if (name.equals(categoryData.name)) {
                return Optional.of(tr);
            }
        }
        return Optional.empty();
    }
}
