package lesson6.helpers;

import lesson6.ApplicationManager;

public class NavigationHelper extends HelperBase {
    private final String baseUrl;

    public NavigationHelper(ApplicationManager manager, String baseUrl) {
        super(manager);
        this.baseUrl = baseUrl;
    }

    public void openLoginPageIfNeeded() {
        String loginPageUrl = baseUrl + "login";
        if (!loginPageUrl.equals(driver.getCurrentUrl())) {
            openPage(loginPageUrl);
        }
    }

    public void openPersonalInformationPage() {
        openPage(baseUrl + "settings");
    }

    public void openAddCategoryPage() {
        openPage(baseUrl + "categories/new");
    }

    public void openCategoriesPage() {
        openPage(baseUrl + "categories");
    }
}
