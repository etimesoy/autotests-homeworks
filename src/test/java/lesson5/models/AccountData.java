package lesson5.models;

public class AccountData {
    public String email;
    public String username;
    public String password;

    public AccountData(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }
}
