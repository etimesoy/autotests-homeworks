package lesson6.tests;

import lesson6.models.AccountData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AuthTests extends TestBase {
    @BeforeEach
    public void beforeEach() {
        app.auth.logoutIfNeeded();
    }

    @Test
    public void loginWithValidData() {
        // given
        AccountData accountData = defaultUser;

        // when
        loginIfNeeded(accountData);

        // then
        Assertions.assertTrue(app.auth.isLogged());
        app.navigation.openPersonalInformationPage();
        Assertions.assertEquals(accountData.email, app.auth.getCurrentAccountEmail());
        Assertions.assertEquals(accountData.username, app.auth.getCurrentAccountUsername());
    }

    @Test
    public void loginWithInvalidData() {
        // given
        AccountData accountData = new AccountData("wrong@gmail.com", "wrong", "fake");

        // when
        loginIfNeeded(accountData);

        // then
        Assertions.assertFalse(app.auth.isLogged());
    }
}
