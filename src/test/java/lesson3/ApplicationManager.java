package lesson3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ApplicationManager {
    public WebDriver driver;

    public NavigationHelper navigation;
    public LoginHelper auth;
    public CartHelper cart;

    public ApplicationManager() {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        String baseUrl = "http://automationpractice.com/";
        navigation = new NavigationHelper(this, baseUrl);
        auth = new LoginHelper(this);
        cart = new CartHelper(this);
    }

    public void stop() {
        driver.quit();
    }
}
