package lesson4.helpers;

import lesson4.ApplicationManager;
import lesson4.models.CartData;
import lesson4.models.OrderData;
import lesson4.models.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Optional;

public class CartHelper extends HelperBase {
    public CartHelper(ApplicationManager manager) {
        super(manager);
    }

    public CartData getCartData() {
        String quantity = getInputElementText(By.xpath("//table[@id='cart_summary']/tbody/tr/td[5]/input[2]"));
        String sizeString = getElementText(By.xpath("//table[@id='cart_summary']/tbody/tr/td[2]/small[2]/a"));
        char size = sizeString.charAt(sizeString.length() - 1);
        OrderData orderData = new OrderData(Integer.parseInt(quantity), OrderData.Size.valueOf(String.valueOf(size)));

        String name = getElementText(By.xpath("//table[@id='cart_summary']/tbody/tr/td[2]/p/a"));
        ProductData productData = new ProductData(name);

        return new CartData(orderData, productData);
    }

    public boolean deleteProductFromCart(ProductData productData) {
        Optional<WebElement> tr = getProductWebElement(productData);
        tr.ifPresent(element -> element.findElement(By.xpath("./td[7]/div/a")).click());
        waitForSeconds(5);
        return tr.isPresent();
    }

    public boolean isProductInCart(ProductData productData) {
        driver.navigate().refresh();
        return getProductWebElement(productData).isPresent();
    }

    // Private

    private Optional<WebElement> getProductWebElement(ProductData productData) {
        Optional<WebElement> tbody = saveFindElement(By.xpath("//table[@id='cart_summary']/tbody"));
        if (tbody.isEmpty()) {
            return Optional.empty();
        }
        for (WebElement tr : tbody.get().findElements(By.xpath("./tr"))) {
            String name = tr.findElement(By.xpath("./td[2]/p/a")).getText();
            if (name.equals(productData.name)) {
                return Optional.of(tr);
            }
        }
        return Optional.empty();
    }
}
