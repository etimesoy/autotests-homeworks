package lesson6.tests;

import lesson6.ApplicationManager;
import lesson6.Settings;
import lesson6.models.AccountData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestBase {
    protected ApplicationManager app;
    protected AccountData defaultUser;

    @BeforeAll
    protected void setUp() {
        app = ApplicationManager.getInstance();
        defaultUser = Settings.getAccountData();
    }

    protected void loginIfNeeded() {
        loginIfNeeded(defaultUser);
    }

    protected void loginIfNeeded(AccountData user) {
        if (!app.auth.isLogged()) {
            app.navigation.openLoginPageIfNeeded();
            app.auth.login(user);
        }
    }
}
