package lesson5;

import lesson5.generators.CategoryDataGenerator;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class Main {
    // первый аргумент - количество категорий, второй аргумент - название файла
    public static void main(String @NotNull [] args) {
        int amount = Integer.parseInt(args[0]);
        String fileName = args[1];
        CategoryDataGenerator generator = new CategoryDataGenerator(new Random());
        generator.generateCategoryData(fileName, amount);
    }
}
