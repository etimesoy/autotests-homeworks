package lesson6.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import lesson6.models.CategoryData;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class CategoryTests extends AuthBase {
    private static final String pathToFile = "test_data.json";

    private static @NotNull Stream<CategoryData> categoryDataProviderFactory() throws IOException {
        File file = new File(pathToFile);
        if (!file.exists()) {
            String description = String.format("Не найден файл с тестовыми данными по пути %s", pathToFile);
            throw new FileNotFoundException(description);
        }

        CategoryData[] categories = (new ObjectMapper()).readValue(file, CategoryData[].class);
        return Arrays.stream(categories);
    }

    @ParameterizedTest
    @MethodSource("categoryDataProviderFactory")
    public void addCategoryTest(CategoryData categoryData) {
        // given
//        loginIfNeeded();
        app.navigation.openAddCategoryPage();

        // when
        app.category.addCategory(categoryData);

        // then
        app.navigation.openCategoriesPage();
        ArrayList<CategoryData> categories = app.category.getCategories();
        Assertions.assertTrue(categories.contains(categoryData));
    }

    @ParameterizedTest
    @MethodSource("categoryDataProviderFactory")
    public void deleteCategoryTest(CategoryData categoryData) {
        // given
//        loginIfNeeded();
        app.navigation.openCategoriesPage();

        // when
        boolean success = app.category.deleteCategory(categoryData);

        // then
        if (success) {
            Assertions.assertFalse(app.category.isCategoryPresent(categoryData));
        } else {
            Assertions.fail(String.format("В списке категорий нет категории \"%s\"", categoryData.name));
        }
    }
}
