package lesson3;

import org.junit.After;
import org.junit.Before;

public class TestBase {
    protected ApplicationManager app;
    protected AccountData user;

    @Before
    public void setUp() {
        app = new ApplicationManager();
        user = new AccountData("ruslangazizov21@mail.ru", "qwerty");
    }

    @After
    public void tearDown() {
        app.stop();
    }
}
