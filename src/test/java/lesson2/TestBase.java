package lesson2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TestBase {
    protected WebDriver driver;
    final private String baseUrl = "http://automationpractice.com/";

    public void openLoginPage() {
        driver.get(baseUrl + "index.php?controller=authentication&back=my-account");
    }

    public void login(AccountData user) {
        driver.findElement(By.id("email")).sendKeys(user.username);
        driver.findElement(By.id("passwd")).sendKeys(user.password);
        driver.findElement(By.cssSelector("#SubmitLogin > span")).click();
    }

    public void goToProductPage(ProductData product) {
        driver.get(baseUrl + "index.php?id_product=" + product.id + "&controller=product");
    }

    public void addCurrentProductToCart() {
        driver.findElement(By.cssSelector(".exclusive > span")).click();
    }
}
