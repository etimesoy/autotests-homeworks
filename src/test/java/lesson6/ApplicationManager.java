package lesson6;

import lesson6.helpers.CategoryHelper;
import lesson6.helpers.LoginHelper;
import lesson6.helpers.NavigationHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ApplicationManager {
    private static final ThreadLocal<ApplicationManager> app = new ThreadLocal<>();
    public WebDriver driver;
    public NavigationHelper navigation;
    public LoginHelper auth;
    public CategoryHelper category;

    private ApplicationManager() {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        navigation = new NavigationHelper(this, Settings.getBaseUrl());
        auth = new LoginHelper(this);
        category = new CategoryHelper(this);
    }

    public static ApplicationManager getInstance() {
        if (app.get() == null) {
            ApplicationManager newInstance = new ApplicationManager();
            app.set(newInstance);
        }
        return app.get();
    }
}
