package lesson5.models;

import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
public class CategoryData {
    public String name;

    public CategoryData(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (getClass() != obj.getClass()) return false;
        CategoryData other = (CategoryData) obj;
        return Objects.equals(name, other.name);
    }
}
