package lesson5.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AuthTests extends TestBase {
    @Test
    public void loginTest() {
        // given & when
        loginIfNeeded();

        // then
        Assertions.assertTrue(app.auth.isLogged());
        app.navigation.openPersonalInformationPage();
        Assertions.assertEquals(defaultUser.email, app.auth.getCurrentAccountEmail());
        Assertions.assertEquals(defaultUser.username, app.auth.getCurrentAccountUsername());
    }
}
