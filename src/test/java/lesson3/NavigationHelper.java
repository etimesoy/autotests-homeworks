package lesson3;

public class NavigationHelper extends HelperBase {
    private final String baseUrl;

    NavigationHelper(ApplicationManager manager, String baseUrl) {
        super(manager);
        this.baseUrl = baseUrl;
    }

    public void openLoginPage() {
        driver.get(baseUrl + "index.php?controller=authentication&back=my-account");
    }

    public void goToProductPage(ProductData product) {
        driver.get(baseUrl + "index.php?id_product=" + product.id + "&controller=product");
    }
}
