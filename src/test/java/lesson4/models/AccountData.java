package lesson4.models;

public class AccountData {
    public String email;
    public String password;

    public AccountData(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
