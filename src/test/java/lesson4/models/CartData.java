package lesson4.models;

public class CartData {
    public OrderData orderData;
    public ProductData productData;

    public CartData(OrderData orderData, ProductData productData) {
        this.orderData = orderData;
        this.productData = productData;
    }
}
