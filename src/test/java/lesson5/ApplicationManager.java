package lesson5;

import lesson5.helpers.CategoryHelper;
import lesson5.helpers.LoginHelper;
import lesson5.helpers.NavigationHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ApplicationManager {
    private static final ThreadLocal<ApplicationManager> app = new ThreadLocal<>();
    public WebDriver driver;
    public NavigationHelper navigation;
    public LoginHelper auth;
    public CategoryHelper category;

    private ApplicationManager() {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        String baseUrl = "http://127.0.0.1:5000/";
        navigation = new NavigationHelper(this, baseUrl);
        auth = new LoginHelper(this);
        category = new CategoryHelper(this);
    }

    public static ApplicationManager getInstance() {
        if (app.get() == null) {
            ApplicationManager newInstance = new ApplicationManager();
            app.set(newInstance);
        }
        return app.get();
    }
}
