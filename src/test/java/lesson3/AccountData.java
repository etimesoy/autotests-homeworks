package lesson3;

public class AccountData {
    public String username;
    public String password;

    public AccountData(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
