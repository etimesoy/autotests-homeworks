package lesson5.helpers;

import lesson5.ApplicationManager;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Optional;

public class HelperBase {
    protected ApplicationManager manager;
    protected WebDriver driver;

    HelperBase(ApplicationManager manager) {
        this.manager = manager;
        this.driver = manager.driver;
    }

    protected void openPage(String url) {
        driver.get(url);
    }

    protected void clickElement(By by) {
        driver.findElement(by).click();
    }

    protected WebElement findElement(By by) {
        return driver.findElement(by);
    }

    protected Optional<WebElement> saveFindElement(By by) {
        try {
            return Optional.ofNullable(driver.findElement(by));
        } catch (NoSuchElementException e) {
            return Optional.empty();
        }
    }

    protected boolean isElementPresent(By by) {
        return saveFindElement(by).isPresent();
    }

    protected String getInputElementText(By by) {
        return driver.findElement(by).getAttribute("value");
    }
}
