package lesson6.helpers;

import lesson6.ApplicationManager;
import lesson6.models.AccountData;
import org.openqa.selenium.By;

public class LoginHelper extends HelperBase {
    public LoginHelper(ApplicationManager manager) {
        super(manager);
    }

    public void login(AccountData user) {
        findElement(By.id("username")).sendKeys(user.username);
        findElement(By.id("password")).sendKeys(user.password);
        clickElement(By.id("submit"));
    }

    public boolean isLogged() {
        return isElementPresent(By.xpath("/html/body/nav/ul/li/a"));
    }

    public String getCurrentAccountEmail() {
        return getInputElementText(By.id("property_3"));
    }

    public String getCurrentAccountUsername() {
        return getInputElementText(By.id("property_1"));
    }

    public void logoutIfNeeded() {
        if (!isLogged()) return;
        clickElement(By.linkText("Log out"));
    }
}
