package lesson3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HelperBase {
    protected ApplicationManager manager;
    protected WebDriver driver;

    HelperBase(ApplicationManager manager) {
        this.manager = manager;
        this.driver = manager.driver;
    }

    protected void clickElement(By by) {
        driver.findElement(by).click();
    }

    protected WebElement findElement(By by) {
        return driver.findElement(by);
    }
}
