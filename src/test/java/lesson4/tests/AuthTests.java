package lesson4.tests;

import org.junit.Assert;
import org.junit.Test;

public class AuthTests extends TestBase {
    @Test
    public void loginTest() {
        // given & when
        loginIfNeeded();

        // then
        Assert.assertTrue(app.auth.isLogged());
        app.navigation.openPersonalInformationPage();
        Assert.assertEquals(defaultUser.email, app.auth.getCurrentAccountEmail());
    }
}
