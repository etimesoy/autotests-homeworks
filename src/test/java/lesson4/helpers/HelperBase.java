package lesson4.helpers;

import lesson4.ApplicationManager;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Optional;

public class HelperBase {
    protected ApplicationManager manager;
    protected WebDriver driver;
    private final WebDriverWait driverWait;

    HelperBase(ApplicationManager manager) {
        this.manager = manager;
        this.driver = manager.driver;
        driverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    protected void openPage(String url) {
        driver.get(url);
    }

    protected void clickElement(By by) {
        driver.findElement(by).click();
    }

    protected WebElement findElement(By by) {
        return driver.findElement(by);
    }

    protected Optional<WebElement> saveFindElement(By by) {
        try {
            return Optional.ofNullable(driver.findElement(by));
        } catch (NoSuchElementException e) {
            return Optional.empty();
        }
    }

    protected boolean isElementPresent(By by) {
        return saveFindElement(by).isPresent();
    }

    protected String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    protected String getInputElementText(By by) {
        return driver.findElement(by).getAttribute("value");
    }

    protected void waitForSeconds(long seconds) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(seconds));
    }

    protected void waitForElementToBeClickable(By by) {
        driverWait.until(ExpectedConditions.elementToBeClickable(by));
    }
}
