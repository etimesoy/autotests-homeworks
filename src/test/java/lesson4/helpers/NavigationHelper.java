package lesson4.helpers;

import lesson4.ApplicationManager;
import lesson4.models.ProductData;
import org.openqa.selenium.By;

public class NavigationHelper extends HelperBase {
    private final String baseUrl;

    public NavigationHelper(ApplicationManager manager, String baseUrl) {
        super(manager);
        this.baseUrl = baseUrl;
    }

    public void openLoginPageIfNeeded() {
        String loginPageUrl = baseUrl + "index.php?controller=authentication";
        if (!loginPageUrl.equals(driver.getCurrentUrl())) {
            openPage(loginPageUrl);
        }
    }

    public void openHomePage() {
        openPage(baseUrl);
    }

    public void openPersonalInformationPage() {
        openPage(baseUrl + "index.php?controller=identity");
    }

    public void openProductPage(ProductData productData) {
        String xpath = String.format("//img[@alt='%s']", productData.name);
        clickElement(By.xpath(xpath));
    }

    public void openCartPage() {
        openPage(baseUrl + "index.php?controller=order");
    }
}
