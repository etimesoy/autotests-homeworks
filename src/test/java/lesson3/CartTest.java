package lesson3;

import org.junit.Test;

public class CartTest extends TestBase {
    @Test
    public void auth() {
        app.navigation.openLoginPage();
        app.auth.login(user);
    }

    @Test
    public void addProductToCart() {
        ProductData product = new ProductData("1");

        app.navigation.openLoginPage();
        app.auth.login(user);
        app.navigation.goToProductPage(product);
        app.cart.addCurrentProductToCart();
    }
}
