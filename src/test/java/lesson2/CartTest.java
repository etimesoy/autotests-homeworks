package lesson2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class CartTest extends TestBase {
    private AccountData user;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
        user = new AccountData("ruslangazizov21@mail.ru", "qwerty");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void auth() {
        openLoginPage();
        login(user);
    }

    @Test
    public void addProductToCart() {
        ProductData product = new ProductData("1");

        openLoginPage();
        login(user);
        goToProductPage(product);
        addCurrentProductToCart();
    }
}
