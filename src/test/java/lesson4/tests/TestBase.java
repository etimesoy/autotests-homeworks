package lesson4.tests;

import lesson4.ApplicationManager;
import lesson4.models.AccountData;
import org.junit.Before;

public class TestBase {
    protected ApplicationManager app;
    protected AccountData defaultUser;

    @Before
    public void setUp() {
        app = ApplicationManager.getInstance();
        defaultUser = new AccountData("ruslangazizov21@mail.ru", "qwerty");
    }

    protected void loginIfNeeded() {
        loginIfNeeded(defaultUser);
    }

    protected void loginIfNeeded(AccountData user) {
        if (!app.auth.isLogged()) {
            app.navigation.openLoginPageIfNeeded();
            app.auth.login(user);
        }
    }
}
