package lesson4;

import lesson4.helpers.CartHelper;
import lesson4.helpers.OrdersHelper;
import lesson4.helpers.LoginHelper;
import lesson4.helpers.NavigationHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ApplicationManager {
    private static final ThreadLocal<ApplicationManager> app = new ThreadLocal<>();
    public WebDriver driver;
    public NavigationHelper navigation;
    public LoginHelper auth;
    public CartHelper cart;
    public OrdersHelper orders;

    private ApplicationManager() {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        String baseUrl = "http://automationpractice.com/";
        navigation = new NavigationHelper(this, baseUrl);
        auth = new LoginHelper(this);
        cart = new CartHelper(this);
        orders = new OrdersHelper(this);
    }

    public static ApplicationManager getInstance() {
        if (app.get() == null) {
            ApplicationManager newInstance = new ApplicationManager();
            app.set(newInstance);
        }
        return app.get();
    }
}
