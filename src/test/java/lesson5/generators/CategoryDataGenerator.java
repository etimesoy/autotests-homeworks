package lesson5.generators;

import com.fasterxml.jackson.databind.ObjectMapper;
import lesson5.models.CategoryData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class CategoryDataGenerator {
    private final Random random;
    public CategoryDataGenerator(Random random) {
        this.random = random;
    }

    public void generateCategoryData(String path, int amount) {
        ArrayList<CategoryData> categories = new ArrayList<>();
        String[] names = {"transport", "groceries", "shopping", "health", "restaurants", "household"};
        for (int i = 0; i < amount; i++) {
            String name = names[random.nextInt(names.length)];
            categories.add(new CategoryData(name));
        }

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(path), categories);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
